// Created on `date`" > $file
#define CALLSIGN "M17-HWN" // Must be M17- and then three uppercase letters/numbers
#define MODULES "A"
#define DHT_BOOT_STRAP "m17-usa.openquad.net"
#define LISTEN_IPV4 "0.0.0.0"
#define IPV6_ADDRESS "none"
#define M17_PORT 17000
#define DASHBOARD_URL "http://44.34.132.17"
#define EMAIL_ADDRESS "netops@memhamwan.org"
#define MCLIENTS
